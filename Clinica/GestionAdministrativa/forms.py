from django import forms
from bootstrap_datepicker_plus import DatePickerInput
from django.forms import ModelForm, ModelChoiceField
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import Paciente, Medico, Clinica, Administrativo

OPCIONES = (
    ('1', 'Aceptar'),
)

class Registro(UserCreationForm):
    email = forms.EmailField(required=False)
    telefono = forms.IntegerField(required=False)
    nombre = forms.CharField(required=True)
    apellidos = forms.CharField(required=True)
    aceptar = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                    choices=OPCIONES) 

    class Meta:
        model = User
        fields = ['nombre', 'apellidos', 'username', 'email', 'telefono', 'password1', 'password2']

class InicioSesionForm(AuthenticationForm):
    remember_me = forms.BooleanField()

class PacienteForm(ModelForm):
    nombre = forms.CharField(required=False)
    apellidos = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    telefono = forms.IntegerField(required=False)

    class Meta:
        model = Paciente
        fields = ['nombre', 'apellidos', 'email', 'telefono']

class PreferenciasForm(ModelForm):
    class Meta:
        model = Paciente
        fields = ['enviar_correo', 'enviar_telegram']
    
class RegistroMedico(UserCreationForm):
    email = forms.EmailField(required=True)
    nombre = forms.CharField(required=True)
    apellidos = forms.CharField(required=True)
    especialidad = forms.CharField(required=True)
    num_colegiado = forms.IntegerField(required=True)
    clinica = forms.ModelChoiceField(queryset=Clinica.objects.all())
    sala = forms.IntegerField(required=True)

    class Meta:
        model = User
        fields = ["nombre", "apellidos", "username", "email", 
                "especialidad", "num_colegiado", "clinica", "sala", "password1", "password2"]    

class MedicoForm(ModelForm):
    nombre = forms.CharField(required=False)
    apellidos = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    especialidad = forms.CharField(required=False)
    num_colegiado = forms.IntegerField(required=False)

    class Meta:
        model = Medico
        fields = ['nombre', 'apellidos', 'email', 'especialidad', 'num_colegiado', 'clinica', 'sala']    

class RegistroClinica(ModelForm):
    direccion = forms.CharField(required=True)
    telefono = forms.IntegerField(required=True)
    nombre = forms.CharField(required=True)

    class Meta:
        model = Clinica
        fields = ['direccion', 'telefono', 'nombre', 'salas_disponibles']

class RegistroAdministrativo(UserCreationForm):
    email = forms.EmailField(required=True)
    nombre = forms.CharField(required=True)
    apellidos = forms.CharField(required=True)
    clinica = forms.ModelChoiceField(queryset=Clinica.objects.all())

    class Meta:
        model = User
        fields = ['nombre', 'apellidos', 'username', 'email', 
                'clinica', 'password1', 'password2']

class AdministrativoForm(ModelForm):
    nombre = forms.CharField(required=False)
    apellidos = forms.CharField(required=False)
    email = forms.EmailField(required=False)

    class Meta:
        model = Administrativo
        fields = ['nombre', 'apellidos', 'email', 'clinica']

class Contacto(forms.Form):
    asunto = forms.CharField()
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)
    aceptar = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                    choices=OPCIONES)
