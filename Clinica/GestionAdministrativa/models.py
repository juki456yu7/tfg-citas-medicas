from django.db import models
from django.contrib.auth.models import User

class Paciente(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    nombre = models.CharField(max_length=50, default="")
    apellidos = models.CharField(max_length=100, default="")
    email = models.EmailField(default="", null=True)
    telefono = models.IntegerField(default=0, null=True)
    chatid = models.CharField(max_length=100, null=True)
    enviar_correo = models.BooleanField(default=True)
    enviar_telegram = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre + " " + self.apellidos

class Clinica(models.Model):
    direccion = models.CharField(max_length=200, default="")
    telefono = models.IntegerField(default=0, unique=True)
    nombre = models.CharField(max_length=100, default="")
    salas_disponibles = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre

class Medico(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50, default="")
    apellidos = models.CharField(max_length=100, default="")
    email = models.EmailField(default="")
    especialidad = models.CharField(max_length=70, default="")
    num_colegiado = models.IntegerField(default=0, unique=True)
    clinica = models.ForeignKey(Clinica, on_delete=models.PROTECT, default=None, null=True)
    sala = models.IntegerField(default=0)

    def __str__(self):
        return self.apellidos + ", " + self.nombre + " (" + self.especialidad + ") - " + str(self.clinica)


class Administrativo(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    clinica = models.OneToOneField(Clinica, on_delete=models.PROTECT, default=None)
    nombre = models.CharField(max_length=50, default="")
    apellidos = models.CharField(max_length=100, default="")
    email = models.EmailField(default="")

    def __str__(self):
        return self.nombre + " " + self.apellidos

class Jornada(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    hora_entrada = models.TimeField()
    hora_salida = models.TimeField()
    fecha = models.DateField()
    horas_trabajadas = models.TimeField()

    def __str__(self):
        return self.usuario.username