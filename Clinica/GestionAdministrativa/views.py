from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import Group, User
from django.template import RequestContext
from .models import Paciente, Medico, Administrativo, Clinica, Jornada
from GestionCitas.models import Cita
from .forms import Registro, Contacto, PacienteForm, RegistroMedico, MedicoForm, RegistroClinica, RegistroAdministrativo, AdministrativoForm, InicioSesionForm, PreferenciasForm
from GestionCitas.forms import CitaForm
from django.core.mail import send_mail
from decouple import config
from django.http import HttpResponse, HttpResponseRedirect
from django.core.management import call_command
from datetime import datetime, date, time, timedelta
from subprocess import call
from .resources import ClinicaResource, PacienteResource, MedicoResource, AdministrativoResource, UserResource
from tablib import Dataset
from decouple import config
import telebot
import json
import requests
import numpy as np

# Token bot telegram 
TOKEN = config('TOKEN')
URL = "https://api.telegram.org/bot" + TOKEN + "/"
tb = telebot.TeleBot(TOKEN)

def inicio(request):
    return render (request, 'GestionCitas/index.html')

def cuadro_medico(request):
    medicos = Medico.objects.all()
    especialidad = []

    for m in medicos:
        especialidad.append(m.especialidad)

    especialidad = np.unique(especialidad)

    if request.POST.get("especialidad"):
        medicos = medicos.filter(especialidad__icontains=request.POST.get("especialidad"))

    return render(request,'GestionAdministrativa/cuadro_medico.html', {'medicos': medicos, 
        'especialidad': especialidad})

def fichar_entrada(request):
    usuario = request.user
    fecha = date.today().isoformat()

    ahora = datetime.now().replace(microsecond=0)

    hora_entrada = ahora.time()
    hora_salida = ahora.time()

    horas_trabajadas = ahora - ahora
    horas_trabajadas = (datetime.min + horas_trabajadas).time()

    jornada = Jornada(usuario=usuario, hora_entrada=hora_entrada, hora_salida=hora_salida, fecha=fecha, horas_trabajadas=horas_trabajadas)
    jornada.save()

    return redirect('/')

def fichar_salida(request):
    usuario = request.user
    fecha = date.today().isoformat()
    ahora = datetime.now().replace(microsecond=0)

    jornada = Jornada.objects.filter(usuario__id__icontains=usuario.id, fecha__icontains=fecha)

    for j in jornada:
        j.hora_salida = ahora.time()

        fecha = date.today()
        aux = datetime.combine(fecha, j.hora_entrada)
        aux2 = ahora - aux

        j.horas_trabajadas = (datetime.min + aux2).time()

        j.save()

    return redirect('/')

def inicio_sesion(request):

    form = InicioSesionForm()

    if request.method == "POST":
        form = InicioSesionForm(data=request.POST)

        if form.is_valid():
            nombre = form.cleaned_data['username']
            contraseña = form.cleaned_data['password']
            remember_me = form.cleaned_data['remember_me']

            usuario = authenticate(username=nombre, password=contraseña)

            if usuario is not None:
                login(request, usuario)
                if not remember_me:
                    request.session.set_expiry(0)

                if usuario.groups.filter(name='Médico').exists():
                    fichar_entrada(request)
                    return redirect('/administracion/gestion_medica')
                elif usuario.groups.filter(name='Administrativo').exists():
                    fichar_entrada(request)
                    return redirect('/administracion/gestion_administrativa')
                else:
                    return redirect('/administracion/area_personal')

    return render (request, 'GestionAdministrativa/inicio_sesion.html', {'form': form})

def registro(request):

    form = Registro()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    if request.method == "POST":
        form = Registro(data=request.POST)
    
        if form.is_valid():
            usuario = form.save()

            # Los usuarios que se registran ellos mismos son los pacientes (también se puede registrar a través de un administrativo)
            # El resto de roles (administrativo y médico) los crea un administrativo. El 1º administrativo es creado por el admin.
            usuario.groups.add(Group.objects.get(name='Paciente'))
            nombre = form.cleaned_data['nombre']
            apellidos = form.cleaned_data['apellidos']
            email = form.cleaned_data['email']

            paciente = Paciente(usuario=usuario, nombre=nombre, apellidos=apellidos, email=email)
            paciente.save()

            if usuario is not None:
                login(request, usuario)

                return redirect('/')

    return render (request, 'GestionAdministrativa/registro.html', {'form': form})

def fin_sesion(request):
    fichar_salida(request)
    logout(request)
    return redirect('/')
    
def editar_usuario(request):
    aux = Paciente.objects.get(usuario_id=request.user.id)
    form = PacienteForm(instance=aux)

    if request.method == "POST":
        form = PacienteForm(request.POST, instance=aux)

        if form.is_valid():
            usuario = form.save(commit=False)
            usuario.save()

            return redirect('/administracion/area_personal')

    return render (request, 'GestionAdministrativa/editar_usuario.html', {'form': form})

def eliminar_usuario(request):
    aux = Paciente.objects.get(usuario_id=request.user.id)
    aux2 = User.objects.get(id=request.user.id)

    logout(request)

    aux.delete()
    aux2.delete()

    return redirect('/')

def cambiar_contraseña(request):
    form = PasswordChangeForm(request.user)
    form.fields['new_password1'].help_text = None

    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            usuario = form.save()

            update_session_auth_hash(request, usuario)
            return redirect('/')

    return render (request, 'GestionAdministrativa/contrasena.html', {'form': form})

def contacto(request):

    form = Contacto()
    
    if request.method == "POST":
        form = Contacto(data=request.POST)

        if form.is_valid():
            aux = form.cleaned_data
            aceptado = form.cleaned_data['aceptar']

            if aceptado == ['1']:
                send_mail(aux['asunto'], aux['mensaje'],
                    aux.get('email',''),[config('USER_MAIL')],)
            else:
                return HttpResponse('No se ha enviado porque usted no aceptó el envío')

            return redirect('/')

    
    return render (request, 'GestionAdministrativa/contacto.html', {'form': form})

def gest_admin(request):
    return render (request, 'GestionAdministrativa/gest_admin.html')

def gest_medica(request):
    medico = Medico.objects.get(usuario_id=request.user.id)
    return render (request, 'GestionAdministrativa/gest_medica.html', {'medico': medico})

def area_pers(request):
    return render (request, 'GestionAdministrativa/area_pers.html')

def preferencias(request):
    aux = Paciente.objects.get(usuario_id=request.user.id)
    form = PreferenciasForm(instance=aux)
    if request.method == "POST":
        form = PreferenciasForm(data=request.POST, instance=aux)

        if form.is_valid():
            enviar_correo = form.cleaned_data['enviar_correo']
            enviar_telegram = form.cleaned_data['enviar_telegram']

            paciente = Paciente.objects.get(usuario_id=request.user.id)
            paciente.enviar_correo = enviar_correo
            paciente.enviar_telegram = enviar_telegram
            paciente.save()

            #Redirige a la misma vista en la que se está
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

    return render (request, 'GestionAdministrativa/preferencias.html', {'form': form})

def buscar_paciente(request):
    return render (request, 'GestionAdministrativa/buscar_paciente.html')

def listar_paciente(request):

    q = request.GET["paciente"]
    paciente = Paciente.objects.filter(nombre__icontains=q)
    return render (request, 'GestionAdministrativa/listar_paciente.html' ,{'paciente': paciente, "query": q})

def editar_paciente(request, id_paciente):
    aux = Paciente.objects.get(id=id_paciente)
    form = PacienteForm(instance=aux)

    if request.method == "POST":
        form = PacienteForm(request.POST, instance=aux)

        if form.is_valid():
            aux = form.save(commit=False)
            aux.save()
            return redirect('/administracion/gestion_administrativa')

    return render (request, 'GestionAdministrativa/editar_paciente.html', {'form': form, 'aux': aux})

def borrar_paciente(request, id_paciente):
    aux = Paciente.objects.get(id=id_paciente)
    usuario_id = aux.usuario_id
    aux.delete()

    if usuario_id:
        aux = User.objects.filter(id=usuario_id)
        if aux:
            aux.delete()

    return redirect('/administracion/gestion_administrativa')

def añadir_paciente(request):
    form = PacienteForm()

    if request.method == "POST":
        form = PacienteForm(data=request.POST)
    
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            apellidos = form.cleaned_data['apellidos']
            email = form.cleaned_data['email']
            telefono = form.cleaned_data['telefono']

            paciente = Paciente(nombre=nombre, apellidos=apellidos, email=email, telefono=telefono)
            paciente.save()

            return redirect('/administracion/gestion_administrativa')

    return render (request, 'GestionAdministrativa/anhadir_paciente.html', {'form': form})

def registro_medico(request):

    form = RegistroMedico()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    if request.method == "POST":
        form = RegistroMedico(data=request.POST)
    
        if form.is_valid():
            usuario = form.save()

            usuario.groups.add(Group.objects.get(name='Médico'))
            nombre = form.cleaned_data['nombre']
            apellidos = form.cleaned_data['apellidos']
            email = form.cleaned_data['email']
            especialidad = form.cleaned_data['especialidad']
            num_colegiado = form.cleaned_data['num_colegiado']
            clinica = form.cleaned_data['clinica']
            sala = form.cleaned_data['sala']

            medico = Medico(usuario=usuario, nombre=nombre, apellidos=apellidos, email=email,
                        especialidad=especialidad, num_colegiado=num_colegiado, clinica=clinica, sala=sala)
            medico.save()

            return redirect ('/administracion/gestion_administrativa')

    return render (request, 'GestionAdministrativa/anhadir_medico.html', {'form': form})

def buscar_medico(request):
    return render (request, 'GestionAdministrativa/buscar_medico.html')

def listar_medico(request):

    q = request.GET["medico"]
    medico = Medico.objects.filter(nombre__icontains=q)
    return render (request, 'GestionAdministrativa/listar_medico.html' ,{'medico': medico, "query": q})

def editar_medico(request, id_medico):
    aux = Medico.objects.get(id=id_medico)
    form = MedicoForm(instance=aux)

    if request.method == "POST":
        form = MedicoForm(request.POST, instance=aux)

        if form.is_valid():
            aux = form.save(commit=False)
            aux.save()
            return redirect('/administracion/gestion_administrativa')

    return render (request, 'GestionAdministrativa/editar_medico.html', {'form': form, 'aux': aux})

def borrar_medico(request, id_medico):
    aux = Medico.objects.get(id=id_medico)
    usuario_id = aux.usuario_id
    aux.delete()

    if usuario_id:
        aux = User.objects.filter(id=usuario_id)
        if aux:
            aux.delete()

    return redirect('/administracion/gestion_administrativa')

def añadir_clinica(request):

    form = RegistroClinica()

    if request.method == "POST":
        form = RegistroClinica(data=request.POST)
            
        if form.is_valid():
            direccion = form.cleaned_data['direccion']
            telefono = form.cleaned_data['telefono']
            nombre = form.cleaned_data['nombre']
            salas_disponibles = form.cleaned_data['salas_disponibles']

            clinica = Clinica(direccion=direccion, telefono=telefono, nombre=nombre, 
                        salas_disponibles=salas_disponibles)
            clinica.save()

            return redirect ('/administracion/gestion_administrativa')

    return render(request, 'GestionAdministrativa/anhadir_clinica.html', {'form': form})

def listar_clinica(request):
    clinicas = Clinica.objects.all()

    if request.POST.get("nombre"):
        clinicas = clinicas.filter(nombre__icontains=request.POST.get("nombre"))

    return render(request, 'GestionAdministrativa/listar_clinica.html', {'clinicas': clinicas})

def editar_clinica(request, id_clinica):
    aux = Clinica.objects.get(id=id_clinica)
    form = RegistroClinica(instance=aux)

    if request.method == "POST":
        form = RegistroClinica(request.POST, instance=aux)

        if form.is_valid():
            aux = form.save(commit=False)
            aux.save()
            return redirect('/administracion/listar_clinica')

    return render (request, 'GestionAdministrativa/editar_clinica.html', {'form': form, 'aux': aux}) 

def borrar_clinica(request, id_clinica):
    aux = Clinica.objects.get(id=id_clinica)
    aux.delete()

    return redirect('/administracion/listar_clinica')

def añadir_administrativo(request):

    form = RegistroAdministrativo()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    if request.method == "POST":
        form = RegistroAdministrativo(data=request.POST)

        if form.is_valid():
            usuario = form.save()

            usuario.groups.add(Group.objects.get(name='Administrativo'))
            nombre = form.cleaned_data['nombre']
            apellidos = form.cleaned_data['apellidos']
            email = form.cleaned_data['email']
            clinica = form.cleaned_data['clinica']

            administrativo = Administrativo(usuario=usuario, clinica=clinica, nombre=nombre, apellidos=apellidos, email=email)
            administrativo.save()

            return redirect ('/administracion/gestion_administrativa')

    return render (request, 'GestionAdministrativa/anhadir_administrativo.html', {'form': form})

def listar_administrativo (request):
    administrativos = Administrativo.objects.all()

    if request.POST.get("nombre"):
        administrativos = administrativos.filter(nombre__icontains=request.POST.get("nombre"))

    return render(request, 'GestionAdministrativa/listar_administrativo.html', {'administrativos': administrativos})

def editar_administrativo(request, id_administrativo):
    aux = Administrativo.objects.get(id=id_administrativo)
    form = AdministrativoForm(instance=aux)

    if request.method == "POST":
        form = AdministrativoForm(request.POST, instance=aux)

        if form.is_valid():
            aux = form.save(commit=False)
            aux.save()
            return redirect('/administracion/listar_administrativo')

    return render (request, 'GestionAdministrativa/editar_administrativo.html', {'form': form, 'aux': aux})

def borrar_administrativo(request, id_administrativo):
    aux = Administrativo.objects.get(id=id_administrativo)
    usuario_id = aux.usuario_id
    aux.delete()

    if usuario_id:
        aux = User.objects.filter(id=usuario_id)
        if aux:
            aux.delete()

    return redirect('/administracion/listar_administrativo')

def confirmar_cita(request, id_cita):
    cita = Cita.objects.get(id=id_cita)
    cita.confirmada = True
    cita.save()

    #Redirige a la misma vista en la que se está
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

def confirmar_llamada_medico(request, id_cita):
    fecha = date.today().isoformat()
    cita = Cita.objects.get(id=id_cita)
    cita.llamada_medico = True

    # El orden de llamada sale del número de confirmados que haya
    aux = Cita.objects.filter(medico__id__icontains=cita.medico_id, fecha__icontains=fecha, confirmada=True)
    cita.orden_llamada = len(aux)

    cita.save()
    paciente = Paciente.objects.get(id=cita.paciente_id)

    if paciente.chatid and (paciente.enviar_telegram == True):
        texto = "Por favor, acuda a la sala " + str(cita.sala)
        tb.send_message(paciente.chatid, texto)

    #Redirige a la misma vista en la que se está
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

def confirmar_atencion(request, id_cita):
    cita = Cita.objects.get(id=id_cita)
    cita.atendido = True
    cita.save()

    #Redirige a la misma vista en la que se está
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

def llamada_automatizada(request, id_medico):
    fecha = date.today().isoformat()
    cita = Cita.objects.filter(medico__id__icontains=id_medico, fecha__icontains=fecha)

    for c in cita:
        if c.confirmada and not c.llamada_medico:
            confirmar_llamada_medico(request, c.id)
            c = Cita.objects.get(id=c.id)
            
            aux = c.orden_llamada - 1
            if aux == 0:
                continue
            else:
                c_aux = Cita.objects.filter(medico__id__icontains=id_medico, fecha__icontains=fecha,
                    orden_llamada=aux)
                for a in c_aux:
                    confirmar_atencion(request, a.id)

            break

    return redirect('/administracion/gestion_medica') 

def eliminar_llamada_medico(request, id_medico):
    fecha = date.today().isoformat()
    cita = Cita.objects.filter(medico__id__icontains=id_medico, fecha__icontains=fecha)

    for c in cita:
        if c.confirmada and c.llamada_medico and not c.atendido:
            c.llamada_medico = False
            c.save()
            break

    return redirect('/administracion/gestion_medica')

def restaurar_bd(request):
    try:
        call_command('dbrestore', verbosity=0, interactive=False)
        return redirect('/administracion/gestion_administrativa')
    except:
        pass

def importar_exportar(request):
    return render(request,'GestionAdministrativa/importar_exportar.html')

def importar_clinica(request):
    if request.method == "POST":
        clinica = ClinicaResource()
        dataset = Dataset()

        clinicas = request.FILES['myfile']
        dataset.load(clinicas.read().decode())

        aux = clinica.import_data(dataset, dry_run=True)

        if not aux.has_errors():
            clinica.import_data(dataset, dry_run=False)

    return render(request, 'GestionAdministrativa/importar_datos.html')

def importar_paciente(request):
    if request.method == "POST":
        paciente = PacienteResource()
        dataset = Dataset()

        pacientes = request.FILES['myfile']
        dataset.load(pacientes.read().decode())

        aux = paciente.import_data(dataset, dry_run=True)

        if not aux.has_errors():
            paciente.import_data(dataset, dry_run=False)

    return render(request, 'GestionAdministrativa/importar_datos.html')

def importar_medico(request):
    if request.method == "POST":
        medico = MedicoResource()
        dataset = Dataset()

        medicos = request.FILES['myfile']
        dataset.load(medicos.read().decode())

        aux = medico.import_data(dataset, dry_run=True)

        if not aux.has_errors():
            medico.import_data(dataset, dry_run=False)

    return render(request, 'GestionAdministrativa/importar_datos.html')

def importar_administrativo(request):
    if request.method == "POST":
        administrativo = AdministrativoResource()
        dataset = Dataset()

        administrativos = request.FILES['myfile']
        dataset.load(administrativos.read().decode())

        aux = administrativo.import_data(dataset, dry_run=True)

        if not aux.has_errors():
            administrativo.import_data(dataset, dry_run=False)

    return render(request, 'GestionAdministrativa/importar_datos.html')

def importar_usuario(request):
    if request.method == "POST":
        usuario = UserResource()
        dataset = Dataset()

        usuarios = request.FILES['myfile']
        dataset.load(usuarios.read().decode())

        aux = usuario.import_data(dataset, dry_run=True)

        if not aux.has_errors():
            usuario.import_data(dataset, dry_run=False)

    return render(request, 'GestionAdministrativa/importar_datos.html')

def exportar_clinica(request):
    clinicas = ClinicaResource()
    dataset = clinicas.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="clinicas.csv"'
    return response

def exportar_paciente(request):
    pacientes = PacienteResource()
    dataset = pacientes.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="pacientes.csv"'
    return response

def exportar_medico(request):
    medicos = MedicoResource()
    dataset = medicos.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="medicos.csv"'
    return response

def exportar_administrativo(request):
    administrativos = AdministrativoResource()
    dataset = administrativos.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="administrativos.csv"'
    return response

def exportar_usuario(request):
    usuarios = UserResource()
    dataset = usuarios.export()
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="usuarios.csv"'
    return response

def añadir_chatid(request, id_paciente):
    paciente = Paciente.objects.get(usuario_id=id_paciente)
    respuesta = requests.get(URL + "getUpdates")
    aux = respuesta.content.decode("utf8")
    mensajes = json.loads(aux)

    # Último mensaje recibido, de ahí sacamos el chatid del paciente
    indice = len(mensajes["result"])-1
    chatid = mensajes["result"][indice]["message"]["chat"]["id"]

    paciente.chatid = chatid
    paciente.save()

    return redirect ("/administracion/area_personal")


