from django.test import TestCase
from django.urls import reverse
from http import HTTPStatus
from django.contrib.auth.models import User
from GestionCitas.models import Cita
from GestionAdministrativa.models import Clinica, Paciente, Medico
from datetime import datetime, date, time, timedelta


class ViewsTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.test_clinica = Clinica.objects.create(nombre="Clínica Prueba", telefono=981205060, 
            direccion="Avda Prueba", salas_disponibles=10)
        cls.test_clinica.save()

        cls.test_paciente = Paciente.objects.create(nombre="Pedro", apellidos="Prueba", email="a@a.com", 
            telefono=981471052)
        cls.test_paciente.save()

        usuario = User.objects.create(username="jds", password="asdfg1234")

        cls.test_medico = Medico.objects.create(usuario=usuario, nombre="Alba", apellidos="López", 
            email="jorge.diaz.seoane@udc.es", especialidad="Medicina General", num_colegiado=151501496, 
            clinica=cls.test_clinica, sala=1)
        cls.test_medico.save()

        fecha = date.today().isoformat()

        cls.test_cita = Cita.objects.create(paciente=cls.test_paciente, medico=cls.test_medico, clinica=cls.test_clinica, 
            fecha=fecha, hora=1, confirmada=False, llamada_medico=False, atendido=False, sala=1, 
            codigo="PeAl1")
        cls.test_cita.save()

        cls.test_cita2 = Cita.objects.create(paciente=cls.test_paciente, medico=cls.test_medico, clinica=cls.test_clinica, 
            fecha=fecha, hora=2, confirmada=False, llamada_medico=False, atendido=False, sala=1, 
            codigo="PeAl2")
        cls.test_cita2.save()

    def test_probar_conexion(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_plantilla_correcta(self):
        response = self.client.get(reverse('listar_citas', args=[self.test_paciente.id]))
        self.assertTemplateUsed(response, 'GestionCitas/listar_citas.html')

    def test_confirmar_cita(self):
        response = self.client.post(reverse('confirmar_cita', args=[self.test_cita.id]))
        self.test_cita.refresh_from_db()
        self.assertEqual(self.test_cita.confirmada, True)

    def test_confirmar_llamada_medico(self):
        response = self.client.post(reverse('llamada_medico', args=[self.test_cita.id]))
        self.test_cita.refresh_from_db()
        self.assertEqual(self.test_cita.llamada_medico, True)

    def test_atendido(self):
        response = self.client.post(reverse('atendido', args=[self.test_cita.id]))
        self.test_cita.refresh_from_db()
        self.assertEqual(self.test_cita.atendido, True)

    def test_añadir_editar_cita(self):
        fecha = date.today().isoformat()
        response = self.client.post(reverse('anhadir_cita', args=[self.test_paciente.id]), 
            data={'medico': self.test_medico.id, 'fecha': fecha})

        self.assertEqual(response.status_code, HTTPStatus.OK)

        response = self.client.post(reverse('editar_cita', args=[1]), data={'medico': self.test_medico.id, 
            'fecha': fecha})

        self.assertEqual(response.status_code, HTTPStatus.OK)


    def test_borrar_cita(self):
        response = self.client.post(reverse('borrar_cita' , args=[self.test_cita.id]))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_cita_correlacionada(self):
        response = self.client.post(reverse('cita_correlacionada', args=[self.test_cita2.id]), 
            data={'cita_correlacionada': self.test_cita.id})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)