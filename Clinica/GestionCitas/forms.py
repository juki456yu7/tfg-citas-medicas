from django import forms
from bootstrap_datepicker_plus import DatePickerInput
from django.forms import ModelForm, ModelChoiceField
from .models import Cita
from GestionAdministrativa.models import Medico

class CitaForm(ModelForm):
    fecha = DatePickerInput(format='%d-%m-%Y')

    class Meta:
        model = Cita
        fields = ['medico', 'fecha']
        widgets = {
            'fecha': DatePickerInput(format='%d-%m-%Y', options={'daysOfWeekDisabled': [0,6],
            'disabledDates': ['2021-02-16', '2021-03-19', '2021-04-01', '2021-04-02', '2021-05-17', 
            '2021-06-24', '2021-10-12', '2021-11-01', '2021-12-06', '2021-12-08']}),
        }

class CitaCorrelacionadaForm(ModelForm):
    class Meta:
        model = Cita
        fields = ['cita_correlacionada']