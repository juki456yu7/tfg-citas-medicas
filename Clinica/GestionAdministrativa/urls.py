from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.inicio),
    path('cuadro_medico', views.cuadro_medico),
    path('registro', views.registro),
    path('inicio_sesion', views.inicio_sesion, name='inicio_sesion'),
    path('fin_sesion', views.fin_sesion, name='fin_sesion'),

    path('contacto', views.contacto),

    path('gestion_administrativa', views.gest_admin),
    path('gestion_medica', views.gest_medica),
    path('area_personal', views.area_pers),
    path('preferencias', views.preferencias),

    path('editar_usuario', views.editar_usuario),
    path('cambiar_contrasena', views.cambiar_contraseña),
    path('eliminar_usuario', views.eliminar_usuario),

    path('fichar_entrada', views.fichar_entrada, name='fichar_entrada'),
    path('fichar_salida', views.fichar_salida, name='fichar_salida'),

    #Pacientes
    path('buscar_paciente', views.buscar_paciente),
    path('listar_paciente', views.listar_paciente),
    path('anhadir_paciente', views.añadir_paciente, name='anhadir_paciente'),
    path('editar_paciente/<int:id_paciente>', views.editar_paciente, name='editar_paciente'),
    path('borrar_paciente/<int:id_paciente>', views.borrar_paciente, name='borrar_paciente'),
    path('anhadir_chatid/<int:id_paciente>', views.añadir_chatid),

    #Médicos
    path('buscar_medico', views.buscar_medico),
    path('listar_medico', views.listar_medico),
    path('anhadir_medico', views.registro_medico, name='anhadir_medico'),
    path('editar_medico/<int:id_medico>', views.editar_medico, name='editar_medico'),
    path('borrar_medico/<int:id_medico>', views.borrar_medico, name='borrar_medico'),

    #Clínicas
    path('listar_clinica', views.listar_clinica),
    path('anhadir_clinica', views.añadir_clinica, name='anhadir_clinica'),
    path('editar_clinica/<int:id_clinica>', views.editar_clinica, name='editar_clinica'),
    path('borrar_clinica/<int:id_clinica>', views.borrar_clinica, name='borrar_clinica'),

    #Administrativos
    path('listar_administrativo', views.listar_administrativo),
    path('anhadir_administrativo', views.añadir_administrativo, name='anhadir_administrativo'),
    path('editar_administrativo/<int:id_administrativo>', views.editar_administrativo, name='editar_administrativo'),
    path('borrar_administrativo/<int:id_administrativo>', views.borrar_administrativo, name='borrar_administrativo'),

    #Citas
    path('confirmar_cita/<int:id_cita>', views.confirmar_cita, name='confirmar_cita'),
    path('confirmar_llamada_medico/<int:id_cita>', views.confirmar_llamada_medico, name='llamada_medico'),
    path('confirmar_atencion/<int:id_cita>', views.confirmar_atencion, name='atendido'),
    path('llamada_automatizada/<int:id_medico>', views.llamada_automatizada),
    path('eliminar_llamada_medico/<int:id_medico>', views.eliminar_llamada_medico),

    #BBDD
    path('restaurar_bd', views.restaurar_bd),

    #Importaciones
    path('importar_exportar', views.importar_exportar),
    path('importar_clinica', views.importar_clinica),
    path('importar_paciente', views.importar_paciente),
    path('importar_medico', views.importar_medico),
    path('importar_administrativo', views.importar_administrativo),
    path('importar_usuario', views.importar_usuario),
    path('exportar_clinica', views.exportar_clinica),
    path('exportar_paciente', views.exportar_paciente),
    path('exportar_medico', views.exportar_medico),
    path('exportar_administrativo', views.exportar_administrativo),
    path('exportar_usuario', views.exportar_usuario),
]


