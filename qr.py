import qrcode

qr = qrcode.QRCode(
    version = 1,
    error_correction = qrcode.constants.ERROR_CORRECT_H,
    box_size = 10,
    border = 4
)

#Se tiene que modificar cada vez que se lanza ngrok
link = 'http://714c185ccdb5.ngrok.io/seleccionar_citas'

qr.add_data(link)
qr.make(fit=True)

imagen = qr.make_image()

imagen.save('qr.png')

#Sacado de: https://pypi.org/project/qrcode/