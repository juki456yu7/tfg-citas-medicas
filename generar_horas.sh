n=0
m=$1
m2=$2
m3=0

while [ $m -lt $(( $3 )) ]
do
	if [ $m3 -lt 10 ]
	then
		if [ $m2 -lt 10 ]
		then
			echo "($n, '"$m:0$m3" - "$m:0$m2"'),"
		else
			echo "($n, '"$m:0$m3" - "$m:$m2"'),"
		fi
	else
		if [ $m2 == 60 ]
		then
			echo "($n, '"$m:$m3" - "$(($m+1)):00"'),"
		else
			echo "($n, '"$m:$m3" - "$m:$m2"'),"
		fi
	fi
	n=$(( n+1 ))
	if [ $m2 -gt 60 ]
	then
		m=$(( m+1 ))
		m2=$2
		m3=0
	else
		m2=$(( m2+$2 ))
		if [ $m2 -gt 60 ]
		then
			m=$(( m+1 ))
			m2=$2
			m3=0
		else
			m3=$(( m3+$2 ))
		fi
	fi
done


