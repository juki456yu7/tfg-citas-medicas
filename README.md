# TFG Citas Médicas

Esta es una versión inicial funcional con todos las funciones implementadas.

Incluye un script para la generación de la lista de horas (que son los intervalos de duración de las citas médicas). Dicho script espera 3 parámetros de entrada (siendo todos de tipo numérico): hora inicio, intervalo cita y hora fin. La hora inicio y la hora fin deberán ser horas en punto y se ponen sin el :00 (por ejemplo, para las 08:00 se pondría 8 simplemente). El intervalo va en minutos, nuevamente es un número (por ejemplo, para intervalos de 15 minutos, se pondría 15). Un ejemplo de entrada de parámetros de este script sería 8 5 15 (empezar a las 08:00, intervalos de 5 minutos y terminar a las 15:00). La salida de este script se tiene que copiar en el archivo vars.py (en la variable LISTA_HORAS ya incluida en el mismo). Por defecto, la lista incluida es para un horario de 08:00 a 15:00 con citas de 15 minutos de duración. 

Incluye una aplicación (qr.py) para generar el código QR (necesario porque se usa ngrok). Una vez que se lanza ngrok es necesario modificar esa parte en esta aplicación y volver a generar el código, después se añade en Clinica/GestionCitas/static/GestionCitas/img/

Contraseña admin: 1234abcd

Por razones de seguridad el token del bot de Telegram, la secret-key de Django y el email y contraseña están en un archivo aparte (llamado .env) que no se sube a este repositorio. Si alguien desea probarlo deberá crear un archivo .env (en el mismo directorio donde va el manage.py) y añadir los siguientes campos (con sus propias claves):

USER_MAIL=

USER_MAIL_PASSWORD=

SECRET_KEY=

TOKEN=

Aclaraciones:
- SECRET_KEY es la de Django (la cual viene en el archivo settings.py)
- TOKEN es el token del bot de telegram
- El email es necesario para enviar correos electrónicos a los pacientes

Para correr el contenedor de Docker, serán necesarios los siguientes comandos:

- docker pull tfgclinicas2021/tfg_gestion_citas:version_final
- docker run -p 8000:8000 tfgclinicas2021/tfg_gestion_citas:version_final python3 manage.py runserver 0:8000
