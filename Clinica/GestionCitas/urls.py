from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio),
    path('cita/<int:id_paciente>', views.cita),
    path('listar_citas/<int:id_paciente>', views.listar_citas),
    path('listar_citas_admin/<int:id_paciente>', views.listar_citas_admin, name='listar_citas'),

    path('administracion/filtrar_dia_inicio/<int:id_medico>', views.filtrar_dia_inicio),
    path('administracion/filtrar_dia_fin/<int:id_medico>', views.filtrar_dia_fin),
    path('administracion/agenda_dia/<int:id_medico>/<str:fecha>', views.agenda_dia),

    path('administracion/fecha_inicio/<int:id_medico>/<int:opcion>', views.fecha_inicio),
    path('administracion/fecha_fin/<int:id_medico>/<int:opcion>', views.fecha_fin),
    path('administracion/anular_citas/<int:id_medico>/<str:fecha>/<int:opcion>', views.anular_citas),

    path('anhadir_cita/<int:id_paciente>', views.añadir_cita, name='anhadir_cita'),
    path('borrar_cita/<int:id_cita>', views.borrar_cita, name='borrar_cita'),
    path('editar_cita/<int:id_cita>', views.editar_cita, name='editar_cita'),
    path('cita_correlacionada/<int:id_cita>', views.cita_correlacionada, name='cita_correlacionada'),

    path('enviar_recordatorio', views.recordatorio),

    path('hora/<int:id_paciente>', views.hora),
    path('forzar_cita/<int:id_paciente>', views.forzar_cita),
    path('registrar_cita/<int:id_medico>/<int:id_paciente>/<str:fecha>/<str:hora>', views.registrar_cita),

    path('filtro_sala', views.filtro_sala),
    path('sala_espera', views.sala_espera),
    path('qr', views.qr),
    path('confirmar_cita_paciente', views.confirmar_cita_paciente_qr),
    path('seleccionar_citas', views.seleccionar_citas),
]