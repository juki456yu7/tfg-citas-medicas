from django.contrib import admin
from .models import Paciente, Medico, Administrativo, Clinica, Jornada

# Register your models here.

admin.site.register(Paciente)
admin.site.register(Medico)
admin.site.register(Clinica)
admin.site.register(Administrativo)
admin.site.register(Jornada)