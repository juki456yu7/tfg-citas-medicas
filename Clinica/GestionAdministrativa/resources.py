from import_export import resources
from .models import Paciente, Medico, Administrativo, Clinica
from django.contrib.auth.models import User

class PacienteResource(resources.ModelResource):
    class Meta:
        model = Paciente

class MedicoResource(resources.ModelResource):
    class Meta:
        model = Medico

class AdministrativoResource(resources.ModelResource):
    class Meta:
        model = Administrativo

class ClinicaResource(resources.ModelResource):
    class Meta:
        model = Clinica     

class UserResource(resources.ModelResource):
    class Meta:
        model = User