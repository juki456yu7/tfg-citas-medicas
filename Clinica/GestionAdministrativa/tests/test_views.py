from django.test import TestCase
from django.urls import reverse
from http import HTTPStatus
from django.contrib.auth.models import User, Group
from GestionAdministrativa.models import Clinica

class ViewsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_clinica = Clinica.objects.create(nombre="Clínica Prueba", telefono=981205060, 
            direccion="Avda Prueba", salas_disponibles=10)
        cls.test_clinica.save()

        cls.test_group = Group.objects.create(name='Médico')
        cls.test_group.save()

        cls.test_group2 = Group.objects.create(name='Administrativo')
        cls.test_group2.save()

        cls.test_group3 = Group.objects.create(name='Paciente')
        cls.test_group3.save()


    def test_añadir_editar_y_borrar_medico(self):
        response = self.client.post(reverse('anhadir_medico'), data={'nombre': 'Pedro', 
            'apellidos': 'Gonzalez', 'username': 'pg', 'email': 'jorge.diaz.seoane@udc.es', 
            'especialidad': 'Medicica General', 'num_colegiado': 151501245, 'clinica': self.test_clinica.id,
            'sala': 1, 'password1': 'asdfg1234', 'password2': 'asdfg1234'})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('editar_medico', args=[1]), data={'nombre': 'Paco', 
            'apellidos': 'Alvarez', 'email': 'jorge.diaz.seoane@udc.es', 
            'especialidad': 'Medicica General', 'num_colegiado': 151501245, 'clinica': self.test_clinica.id,
            'sala': 1})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('borrar_medico', args=[1]))

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_añadir_editar_y_borrar_administrativo(self):
        response = self.client.post(reverse('anhadir_administrativo'), data={'nombre': 'Alba', 
            'apellidos': 'Campos', 'username': 'ac', 'email': 'jorge.diaz.seoane@udc.es', 
            'clinica': self.test_clinica.id, 'password1': 'asdfg1234', 'password2': 'asdfg1234'})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('editar_administrativo', args=[1]), data={'nombre': 'Laura', 
            'apellidos':'Campos', 'email': 'jorge.diaz.seoane@udc.es', 'clinica': self.test_clinica.id})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('borrar_administrativo', args=[1]))

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_añadir_editar_y_borrar_clinica(self):
        response = self.client.post(reverse('anhadir_clinica'), data={'direccion': 'C/ Cuba 1', 
            'telefono': 981257410, 'nombre': 'Clinica Margarita', 'salas_disponibles': 10})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('editar_clinica', args=[1]), data={'direccion': 'C/ Cuba 2', 
            'telefono': 981257412, 'nombre': 'Clinica Girasol', 'salas_disponibles': 10})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('borrar_clinica', args=[1]))

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_añadir_editar_y_borrar_paciente(self):
        response = self.client.post(reverse('anhadir_paciente'), data={'nombre': 'Juan', 
            'apellidos': 'Fernandez', 'username': 'jf', 'email': 'jorge.diaz.seoane@udc.es', 
            'password1': 'asdfg1234', 'password2': 'asdfg1234'})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('editar_paciente', args=[1]), data={'nombre': 'Pedro', 
        'apellidos': 'Diaz', 'email': 'jorge.diaz.seoane@udc.es', 'telefono': 981247145})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('borrar_paciente', args=[1]))

        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_inicio_sesion(self):
        self.user = User.objects.create_user(username='jds', password='asdfg1234')
        response = self.client.post(reverse('inicio_sesion'), data={'username': 'jds', 
            'password': 'asdfg1234', 'remember_me': True})
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_fin_sesion(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.client.login(username='testuser', password='12345')
        response = self.client.post(reverse('fin_sesion'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_fichaje_jornada(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.client.login(username='testuser', password='12345')

        response = self.client.post(reverse('fichar_entrada'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

        response = self.client.post(reverse('fichar_salida'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        