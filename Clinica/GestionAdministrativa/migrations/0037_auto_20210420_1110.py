# Generated by Django 3.1.4 on 2021-04-20 09:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GestionAdministrativa', '0036_auto_20210318_1033'),
    ]

    operations = [
        migrations.AddField(
            model_name='paciente',
            name='enviar_correo',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='paciente',
            name='enviar_telegram',
            field=models.BooleanField(default=True),
        ),
    ]
