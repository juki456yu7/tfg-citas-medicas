from django.test import TestCase
from django.contrib.auth.models import User
from GestionCitas.models import Cita
from GestionAdministrativa.models import Clinica, Paciente, Medico
from datetime import datetime, date, time, timedelta

class CitaModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        clinica = Clinica.objects.create(nombre="Clínica Prueba", telefono=981205060, 
            direccion="Avda Prueba", salas_disponibles=10)
        paciente = Paciente.objects.create(nombre="Pedro", apellidos="Prueba", email="a@a.com", 
            telefono=981471052)
        usuario = User.objects.create(username="jds", password="asdfg1234")
        medico = Medico.objects.create(usuario=usuario, nombre="Alba", apellidos="López", email="a@a.com", 
            especialidad="Medicina General", num_colegiado=151501496, clinica=clinica, sala=1)
        fecha = date.today().isoformat()
        Cita.objects.create(paciente=paciente, medico=medico, clinica=clinica, fecha=fecha, 
            hora=1, confirmada=False, llamada_medico=False, atendido=False, sala=1, codigo="PeAl1")

    def test_paciente_label(self):
        cita=Cita.objects.get(id=1)
        field_label = cita._meta.get_field('paciente').verbose_name
        self.assertEquals(field_label,'paciente')

    def test_codigo_max_length(self):
        cita=Cita.objects.get(id=1)
        max_length = cita._meta.get_field('codigo').max_length
        self.assertEquals(max_length, 10)