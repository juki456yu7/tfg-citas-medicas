# Generated by Django 3.1.4 on 2020-12-13 23:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('paciente', models.CharField(max_length=70)),
                ('medico', models.CharField(max_length=70)),
                ('fecha', models.DateField()),
                ('lugar', models.CharField(max_length=70)),
            ],
        ),
    ]
