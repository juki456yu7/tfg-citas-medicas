from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils.dateparse import parse_date # Para pasar de str a datetime.date
from datetime import date
import datetime
import copy
from .models import Cita
from Clinica.vars import LISTA_HORAS
from .forms import CitaForm, CitaCorrelacionadaForm
from GestionAdministrativa.models import Paciente, Medico, Clinica
from django.core.mail import send_mail
from decouple import config
from django.contrib.auth.models import User
from mobi.decorators import detect_mobile
from decouple import config
import telebot
import json
import requests

# Token bot telegram
TOKEN = config('TOKEN') 
URL = "https://api.telegram.org/bot" + TOKEN + "/"
tb = telebot.TeleBot(TOKEN)

def inicio(request):
    return render(request,'GestionCitas/index.html')

def cita(request, id_paciente):
    paciente = Paciente.objects.get(usuario_id=id_paciente)
    form = CitaForm()

    if request.method == "POST":
        form = CitaForm(data=request.POST)

    return render (request, 'GestionCitas/cita.html', {'form': form, 'id_paciente': paciente.id})

# Método para añadir citas como administrativo, no es necesario que tenga usuario
def añadir_cita(request, id_paciente):
    paciente = Paciente.objects.get(id=id_paciente)
    form = CitaForm()

    if request.method == "POST":
        form = CitaForm(data=request.POST)

    return render (request, 'GestionCitas/cita.html', {'form': form, 'id_paciente': paciente.id})

def borrar_cita(request, id_cita):
    cita = Cita.objects.get(id=id_cita)
    paciente = Paciente.objects.get(id=cita.paciente_id)
    cita.delete()

    if paciente.usuario_id:
        aux = "/listar_citas/%d" % paciente.usuario_id
        return redirect(aux)
    
    return redirect('/')

def editar_cita(request, id_cita):
    cita = Cita.objects.get(id=id_cita)
    paciente = Paciente.objects.get(id=cita.paciente_id)
    form = CitaForm(instance=cita)
    borrar_cita(request, cita.id) #Borramos la cita anterior, para que no quede almacenada

    if request.method == "POST":
        form = CitaForm(request.POST, instance=cita)

        if form.is_valid():
            form.save()
            aux = "/listar_citas/%d" % paciente.usuario_id
            return redirect(aux)
            
    return render(request, 'GestionCitas/editar_cita.html', {'form': form, 'id_paciente': paciente.id})

def listar_citas(request, id_paciente):
    paciente = Paciente.objects.get(usuario_id=id_paciente)
    cita = Cita.objects.filter(paciente__id__icontains=paciente.id)
    for c in cita:
        c.hora = LISTA_HORAS[c.hora][1]

    return render (request, 'GestionCitas/listar_citas.html', {'cita': cita, 'paciente': paciente.id})

def listar_citas_admin(request, id_paciente):
    cita = Cita.objects.filter(paciente__id__icontains=id_paciente)
    for c in cita:
        c.hora = LISTA_HORAS[c.hora][1]

    return render (request, 'GestionCitas/listar_citas.html', {'cita': cita, 'paciente': id_paciente})

def filtrar_dia_inicio(request, id_medico):
    form = CitaForm()
    return render (request, 'GestionCitas/filtrar_dia_inicio.html', {'form': form, 'id_medico': id_medico})

def filtrar_dia_fin(request, id_medico):
    fecha = request.GET["fecha"]
    form = CitaForm()
    return render (request, 'GestionCitas/filtrar_dia_fin.html', {'form': form, 'fecha': fecha, 
    'id_medico': id_medico})

def agenda_dia(request, id_medico, fecha):
    medico = Medico.objects.get(id=id_medico)
    fecha_inicio = datetime.datetime.strptime(fecha, "%d-%m-%Y").date()
    fecha_fin = request.GET["fecha"]
    fecha_fin = datetime.datetime.strptime(fecha_fin, "%d-%m-%Y").date() 

    cita = Cita.objects.filter(medico__id__icontains=medico.id, fecha__range=[fecha_inicio, fecha_fin])
    for c in cita:
        c.hora = LISTA_HORAS[c.hora][1]

    return render (request, 'GestionCitas/agenda_dia.html', {'cita': cita, 'medico_id': medico.id})

def recordatorio(request):
    aux = datetime.date.today()
    fecha = aux + datetime.timedelta(days=1)
    cita = Cita.objects.filter(fecha__icontains=fecha)
    fecha = fecha.strftime("%d/%m/%Y")

    for c in cita:
        paciente = Paciente.objects.get(id=c.paciente_id)

        if paciente.usuario_id:
            c.hora = LISTA_HORAS[c.hora][1]
            mensaje = 'Le recordamos que tiene usted una cita el día %s a las %s' % (fecha, c.hora)
            usuario = User.objects.get(id=paciente.usuario_id)
            email = usuario.email
            if email and (paciente.enviar_correo == True):
                send_mail('Recordatorio cita', mensaje, email, [email],)

            if paciente.chatid and (paciente.enviar_telegram == True):
                tb.send_message(paciente.chatid, mensaje)

    return redirect('/')

def calendario (id_cita):
    cita = Cita.objects.get(id=id_cita)
    cita.hora = LISTA_HORAS[cita.hora][1]
    fecha = cita.fecha
    fecha = fecha.strftime("%Y%m%dT")
    hora_inicio = cita.hora[0:5]
    hora_inicio = hora_inicio.replace(':', '')
    hora_inicio = int(hora_inicio) - 200 # Factor de corrección para ponerlo en gmt0 (necesario para el link)
    if hora_inicio < 1000:
        hora_inicio = fecha + '0' + str(hora_inicio) + '00Z'
    else:
        hora_inicio = fecha + str(hora_inicio) + '00Z'

    hora_fin = cita.hora[7:13]
    hora_fin = hora_fin.replace(':', '')
    hora_fin = hora_fin.replace(' ', '')
    hora_fin = int(hora_fin) - 200 # Factor de corrección para ponerlo en gmt0 (necesario para el link)
    if hora_fin < 1000:
        hora_fin = fecha + '0' + str(hora_fin) + '00Z'
    else:
        hora_fin = fecha + str(hora_fin) + '00Z'

    link = 'https://www.google.com/calendar/render?action=TEMPLATE&text=Cita&dates=' + hora_inicio + '%2F' + hora_fin

    return link

def hora(request, id_paciente):
    medico = request.POST["medico"]
    paciente = Paciente.objects.get(id=id_paciente)
    fecha = request.POST["fecha"]
    fecha2 = datetime.datetime.strptime(fecha, "%d-%m-%Y").date()
    aux = copy.deepcopy(LISTA_HORAS)
    lista = list(map(list, aux))

    cita = Cita.objects.filter(medico__id__icontains=medico, fecha__icontains=fecha2)

    aviso = False
    aviso_aux = Cita.objects.filter(medico__id__icontains=medico, fecha__icontains=fecha2, paciente__id__icontains=id_paciente) 
    if len(aviso_aux) > 0:
        aviso = True

    for c in cita:
        lista[c.hora][1] = "Ocupada"

    for i in range(len(LISTA_HORAS)):
        if lista[i][1] != "Ocupada":
            lista[i] = aux[i][1]
        else:
            lista[i] = "Ocupada"

    return render(request, 'GestionCitas/hora.html', {'lista': lista, 'fecha': fecha, 'medico': medico, 
        'paciente': paciente.id, 'aviso': aviso})

def forzar_cita(request, id_paciente):
    medico = request.POST["medico"]
    paciente = Paciente.objects.get(id=id_paciente)
    fecha = request.POST["fecha"]
    fecha2 = datetime.datetime.strptime(fecha, "%d-%m-%Y").date()
    aux = copy.deepcopy(LISTA_HORAS)
    lista = list(map(list, aux))

    cita = Cita.objects.filter(medico__id__icontains=medico, fecha__icontains=fecha2)

    for i in range(len(LISTA_HORAS)):
        lista[i] = aux[i][1]

    return render(request, 'GestionCitas/hora.html', {'lista': lista, 'fecha': fecha, 'medico': medico, 'paciente': paciente.id})

def cita_correlacionada(request, id_cita):
    cita = Cita.objects.get(id=id_cita)
    form = CitaCorrelacionadaForm()

    if request.method == "POST":
        form = CitaCorrelacionadaForm(data=request.POST)

        if form.is_valid():
            aux = form.cleaned_data['cita_correlacionada']
            cita.cita_correlacionada = aux
            cita.save()

            return redirect('/administracion/gestion_administrativa')

    return render(request, 'GestionCitas/cita_correlacionada.html', {'form': form})

def registrar_cita(request, id_medico, id_paciente, fecha, hora):
    paciente = Paciente.objects.get(id=id_paciente)
    medico = Medico.objects.get(id=id_medico)
    clinica = medico.clinica
    sala = medico.sala
    fecha = datetime.datetime.strptime(fecha, "%d-%m-%Y").date()

    for i in range(len(LISTA_HORAS)):
        if LISTA_HORAS[i][1] == hora:
            aux = LISTA_HORAS[i][0]

    cita = Cita(paciente=paciente, medico=medico, clinica=clinica, fecha=fecha, hora=aux, confirmada=False,
                sala=sala)
    cita.save()

    link = calendario(cita.id)

    fecha = cita.fecha.strftime("%d/%m/%Y")
    hora = LISTA_HORAS[cita.hora][1]
    mensaje = 'Tiene usted una cita el día %s a las %s' % (fecha, hora)
    mensaje = mensaje + ' ' + link

    if paciente.email and (paciente.enviar_correo == True):
        email = paciente.email
        send_mail('Cita', mensaje, email, [email],)

    # Si el paciente tiene registrado el bot, se le avisa por telegram
    if paciente.chatid and (paciente.enviar_telegram == True):
        tb.send_message(paciente.chatid, mensaje)

        #Queda comentado porque hay que variar la parte de ngrok cada vez que se quiera probar
        #mensaje_confirmar = 'http://c2002640b68a.ngrok.io/seleccionar_citas'
        #tb.send_message(paciente.chatid, mensaje_confirmar)

    return redirect('/')

def fecha_inicio(request, id_medico, opcion):
    form = CitaForm()
    return render (request, 'GestionCitas/fecha_inicio.html', {'form': form, 'id_medico': id_medico, 
                'opcion': opcion})

def fecha_fin(request, id_medico, opcion):
    fecha = request.GET["fecha"]
    form = CitaForm()
    return render (request, 'GestionCitas/fecha_fin.html', {'form': form, 'fecha': fecha, 
    'id_medico': id_medico, 'opcion': opcion})

def anular_citas(request, id_medico, fecha, opcion):
    medico = Medico.objects.get(id=id_medico)
    fecha_inicio = datetime.datetime.strptime(fecha, "%d-%m-%Y").date()
    fecha_fin = request.GET["fecha"]
    fecha_fin = datetime.datetime.strptime(fecha_fin, "%d-%m-%Y").date() 

    cita = Cita.objects.filter(medico__id__icontains=medico.id, fecha__range=[fecha_inicio, fecha_fin])

    # Anular citas y mandar email
    if opcion == 1:
        for c in cita:
            paciente = Paciente.objects.get(id=c.paciente_id)
            email = paciente.email
            if (email and paciente.enviar_correo == True) or (paciente.chatid and paciente.enviar_telegram == True):
                fecha = c.fecha.strftime("%d/%m/%Y")
                hora = LISTA_HORAS[c.hora][1]
                mensaje = 'Su cita del día %s a las %s ha sido anulada, rogamos solicite una nueva' % (fecha, hora)
                if email and (paciente.enviar_correo == True):
                    send_mail('Cita anulada', mensaje, email, [email],)
                
                if paciente.chatid and (paciente.enviar_telegram == True):
                    tb.send_message(paciente.chatid, mensaje)

        for c in cita:
            paciente = Paciente.objects.get(id=c.paciente_id)
            email = paciente.email
            if (email and paciente.enviar_correo == True) or (paciente.chatid and paciente.enviar_telegram == True):
                c.delete()
            else:
                continue
    
    # Reubicar citas
    elif opcion == 2:
        cita = list(cita)
        # Si la fecha final es viernes, pasamos al lunes
        if fecha_fin.weekday() == 4:
            fec_ini = fecha_fin + datetime.timedelta(days=3)
        # Si no lo es, pasamos al día siguiente
        else:
            fec_ini = fecha_fin + datetime.timedelta(days=1)
        fec_fin = "31-12-2021"
        fec_fin = datetime.datetime.strptime(fec_fin, "%d-%m-%Y").date()
        delta = datetime.timedelta(days=1)
        delta3 = datetime.timedelta(days=3)

        # Mientras haya citas en estas fechas
        while len(cita) > 0:
            while fec_ini <= fec_fin:
                aux = copy.deepcopy(LISTA_HORAS)
                lista = list(map(list, aux))
                num_ocupadas=0

                citas = Cita.objects.filter(medico__id__icontains=medico.id, fecha__icontains=fec_ini)

                for c in citas:
                    lista[c.hora][1] = "Ocupada"
                    num_ocupadas += 1
                    

                if num_ocupadas == len(LISTA_HORAS):
                    if fec_ini.weekday() == 4:
                        fec_ini += delta3
                    else:
                        fec_ini += delta
                    continue
                else:
                    for c in range(len(LISTA_HORAS)):
                        if lista[c][1] != "Ocupada":
                            aux = cita[0]
                            cita2 = Cita.objects.get(id=aux.id)
                            cita2.fecha = fec_ini
                            if cita2.hora < 28:
                                cita2.hora = LISTA_HORAS[c][0]
                            else:
                                continue
                            paciente = Paciente.objects.get(id=cita2.paciente_id)
                            email = paciente.email
                            if (email and paciente.enviar_correo == True) or (paciente.chatid and paciente.enviar_telegram == True):
                                fecha = cita2.fecha.strftime("%d/%m/%Y")
                                if cita2.hora < 28:
                                    hora = LISTA_HORAS[cita2.hora][1]
                                else: 
                                    continue
                                mensaje = 'Su cita ha sido cambiada para el día %s a las %s' % (fecha, hora) 
                                if email and (paciente.enviar_correo == True):
                                    send_mail('Cambio cita', mensaje, email, [email],)

                                if paciente.chatid and (paciente.enviar_telegram == True):
                                    tb.send_message(paciente.chatid, mensaje)

                                cita2.save()
                                break
                            else:
                                break
                    
                    break      
              
              
            del cita[0]


    return redirect ('/administracion/gestion_administrativa')

def filtro_sala(request):
    clinicas = Clinica.objects.all()
    return render(request, 'GestionCitas/filtro_sala.html', {'clinicas': clinicas})

def sala_espera(request):
    fecha = datetime.date.today()
    nombre = request.GET['clinica']
    clinica = Clinica.objects.get(nombre=nombre)
    sala_inicio = request.GET['sala_inicio']
    sala_fin = request.GET['sala_fin']
    cita = Cita.objects.filter(fecha__icontains=fecha, clinica__id__icontains=clinica.id, 
        sala__range=[sala_inicio, sala_fin])

    for c in cita:
        paciente = Paciente.objects.get(id=c.paciente_id)
        aux = paciente.nombre[0:2]
        medico = Medico.objects.get(id=c.medico_id)
        aux2 = medico.nombre[0:2]
        c.codigo = str(aux) + str(aux2) + "_" + str(c.hora+1)
        c.save()

    return render (request, 'GestionCitas/sala_espera.html', {'cita': cita, 'sala_inicio': sala_inicio, 
        'sala_fin': sala_fin})

def qr(request):
    return render(request, 'GestionCitas/qr.html')

@detect_mobile
def seleccionar_citas(request):
    if request.mobile:
        paciente = Paciente.objects.get(usuario_id=request.user.id)
        fecha = datetime.date.today()

        cita = Cita.objects.filter(paciente__id__icontains=paciente.id, fecha__icontains=fecha)

        for c in cita:
            c.hora = LISTA_HORAS[c.hora][1]
    
    return render(request, 'GestionCitas/confirmar_cita_qr.html', {'cita': cita})

@detect_mobile
def confirmar_cita_paciente_qr(request):
    if request.mobile:
        citas_seleccionadas = request.POST.getlist('c')
        paciente = Paciente.objects.get(usuario_id=request.user.id)

        for c in citas_seleccionadas:
            cita=Cita.objects.get(id=c)
            cita.confirmada=True
            cita.save()

        if paciente.chatid:
            texto = "Cita/s confirmadas"
            tb.send_message(paciente.chatid, texto)

        return redirect('/')
    else:
        return redirect('/')