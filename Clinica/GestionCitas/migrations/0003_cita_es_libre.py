# Generated by Django 3.1.4 on 2021-01-11 07:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GestionCitas', '0002_auto_20210107_1001'),
    ]

    operations = [
        migrations.AddField(
            model_name='cita',
            name='es_libre',
            field=models.BooleanField(default=True),
        ),
    ]
