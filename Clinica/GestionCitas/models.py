from django.db import models
from GestionAdministrativa.models import Paciente, Medico, Clinica
from decouple import config
from Clinica.vars import LISTA_HORAS

# Create your models here.

class Cita(models.Model):
    class Meta:
        unique_together = ('medico', 'fecha', 'hora', 'paciente')
        ordering = ['fecha', 'hora']

    paciente = models.ForeignKey(Paciente, on_delete=models.PROTECT)
    medico = models.ForeignKey(Medico, on_delete=models.PROTECT)
    clinica = models.ForeignKey(Clinica, on_delete=models.PROTECT)
    fecha = models.DateField(default="")
    hora = models.IntegerField(choices=LISTA_HORAS)
    confirmada = models.BooleanField(default=False)
    llamada_medico = models.BooleanField(default=False)
    atendido = models.BooleanField(default=False)
    sala = models.IntegerField(default=0)
    codigo = models.CharField(max_length=10, null=True)
    cita_correlacionada = models.ForeignKey('self', on_delete=models.CASCADE, null=True, default="")
    orden_llamada = models.IntegerField(default=0)

    def __str__(self):
        return "Paciente: " + self.paciente.nombre + " ; Médico: " + self.medico.nombre
